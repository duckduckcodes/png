import Dropzone from "dropzone";
import ExifReader from "exifreader";
import "dropzone/dist/dropzone.css";
// import * as L from 'leaflet'

const myDropzone = new Dropzone("#my-form");
const output = document.querySelector("#output");
const imageMod = document.querySelector(".image");

function getDescription(tag) {
  if (Array.isArray(tag)) {
    return tag.map((item) => item.description).join(", ");
  }
  return tag.description;
}

/**
 *
 * @param {ExifReader.GpsTags} obj
 */
const gpsFormat = (obj) => {
  let s = "";
  if (obj.Altitude) s += `altitude: ${obj.Altitude.toFixed(4)}. `;
  if (obj.Latitude) s += `latitude: ${obj.Latitude.toFixed(4)}. `;
  if (obj.Longitude) s += `longitude: ${obj.Longitude.toFixed(4)}`;
  return s;
};

/**
 *
 * @param {Dropzone.DropzoneFile} file
 */


// var map = L.map('map').setView([51.505, -0.09], 13);

// L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// }).addTo(map);


// var map = L.map('map').setView([51.505, -0.09], 13);

// L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// }).addTo(map);


myDropzone.on("addedfile", async (file) => {
  const tableBody = document.createElement("table");
  tableBody.className = "table table-striped table-hover";

  const tags = ExifReader.load(await file.arrayBuffer());

  const tagsExpanded = ExifReader.load(await file.arrayBuffer(), {
    expanded: true,
  });
  const gps = tagsExpanded.gps;

  if (gps) {
    let row = document.createElement("tr");
    row.innerHTML = "<td>" + "GPS" + "</td><td>" + gpsFormat(gps) + "</td>";
    tableBody.appendChild(row);

    // map.invalidateSize();
    // if (gps.Latitude && gps.Longitude)
    //     L.marker([gps.Latitude, gps.Longitude]).addTo(map)
    //         .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    //         .openPopup();
  }

  for (const name in tags) {
    let description = getDescription(tags[name]);
    if (description !== undefined) {
      let row = document.createElement("tr");
      row.innerHTML = "<td>" + name + "</td><td>" + description + "</td>";
      tableBody.appendChild(row);
    }
  }

  output.insertBefore(tableBody, output.childNodes[0]);

  const originalImageUrl = file.dataURL;
  // create a new image without the exif data

  stripExifFromImage(originalImageUrl, function (blob) {
    let ima = document.createElement("img");
    ima.classList.add("image");
    ima.src = URL.createObjectURL(blob);


    // add a download button, to download the image

    let downloadBtn = document.createElement("button");
    downloadBtn.classList.add("downloadBtn");
    downloadBtn.innerText = "Download";

    downloadBtn.addEventListener("click", () => {
      var a = document.createElement("a");
      a.href = ima.src;
      a.download = "output.png";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    });

    output.insertBefore(ima, output.childNodes[1]);
    output.insertBefore(downloadBtn, output.childNodes[2]);
  });

  output.innerHTML =
    `<div><b>File added: ${file.name}<b></div>` +
    output.innerHTML
});


//write the image into a new canvas

function stripExifFromImage(imageUrl, callback) {
  const img = new Image();

  img.onload = function () {
    // Create a canvas
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    const context = canvas.getContext("2d");
    context.drawImage(img, 0, 0);

    canvas.toBlob(callback, "image/jpeg");
  };

  img.src = imageUrl;
}
